package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PolskalaTest {
    @Test
    public void factorialTest1() {
        Polskala polskala = new Polskala();
        int n = 1;
        long expectedResult = 1;

        long result = polskala.factorial(n);

        Assertions.assertEquals(expectedResult,result);
    }

    @Test
    public void factorialTest2() {
        Polskala polskala = new Polskala();
        int n = 2;
        long expectedResult = 2;

        long result = polskala.factorial(n);

        Assertions.assertEquals(expectedResult,result);
    }

    @Test
    public void factorialTest3() {
        Polskala polskala = new Polskala();
        int n = 2;
        long expectedResult = 2;

        long result = polskala.factorial(n);

        Assertions.assertEquals(expectedResult,result);
    }
}
