package cz.cvut.fel.ts1;

public class Polskala {
    public long factorial (int n) {
        if(n <= 1){
            return 1;
        }
        else{
            return n*factorial(n-1);
        }
    }
    public static int factorialRecursive(int n){
        if(n <= 1){
            return 1;
        }
        else{
            return n*factorialRecursive(n-1);
        }
    }
}
